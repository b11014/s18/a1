//

function addFunction(num1, num2){
	let sum = num1 + num2
	console.log("The sum of two numbers is ");
	console.log(sum);
};
addFunction(1,2);

//
function subFunction(num1, num2){
	let difference = num1 - num2
	console.log("The difference of two numbers is ");
	console.log(difference);
};
subFunction(4,2);

//
function multiplicationFun(num1, num2){
	return num1 * num2;
};
	let product = multiplicationFun(5,2);
		console.log("The product of two numbers is");
		console.log(product);
//

function divisionFunction(num1, num2){
	return num1 / num2;
};
	let quotient = divisionFunction(10,2);
	console.log("The quotient of two numbers is ");
	console.log(quotient);
//
function radiusOfCircle(radius){
	return 3.141592653589793 * radius * radius
};
	let area = radiusOfCircle(4);
	console.log("The area of a circle with 4 radius is ")
	console.log(area);

//
function averageNumbers(num1, num2, num3, num4) {
	return (num1 + num2 + num3 + num4) / 4
};
let average = averageNumbers(95,50,50,50);
console.log("The average of the given 4 numbers is ");
console.log(average);

//

function averagePercentage(num1, num2){
	let averageTotal = (num2 / num1) * 100
	let isPassed = averageTotal >= 75;
	console.log("Is this a passing score? ");
	console.log(isPassed); 
};
averagePercentage(95,75);

/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication. (use return statement)

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division. (use return statement)

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-formula: area = pi * (radius raised to two) **2
			-look up the use of the exponent operator. (ES6 update)
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation. (use return statement)

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

		Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-formula for average: num1 + num2 + num3 + ... divide by total numbers
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation. (use return statement) 

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/



